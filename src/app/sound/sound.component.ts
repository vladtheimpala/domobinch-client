import { OnInit, Component, Input } from "@angular/core";
import { AppSocketService } from "../app-socket.service";
import { SoundActionCode, SoundEvent } from '../app-models.model';
@Component({
    selector: 'binch-sound',
    templateUrl: './sound.component.html',
    styleUrls: ['./sound.component.scss']
  })
  export class SoundComponent implements OnInit {

    actions: any[];

    @Input() isReady: boolean;
    loaded: boolean;


    constructor(private appSocketService: AppSocketService) {
      this.actions = [
        { code: SoundActionCode.TOGGLE_TURN_ON_OFF, icon: 'power_settings_new'},
        { code: SoundActionCode.SOURCE_BT, icon: 'bluetooth'},
        { code: SoundActionCode.SOURCE_HDMI, icon: 'cast'},
        { code: SoundActionCode.MUTE, icon: 'volume_off'},
        { code: SoundActionCode.VOL_DOWN, icon: 'volume_down'},
        { code: SoundActionCode.VOL_UP, icon: 'volume_up'}

      ]
    }

    ngOnInit() {
      this.loaded = true;
    }

    sendAction(actionCode) {

      console.error('sound action sent:', actionCode)

      const soundEvent = new SoundEvent({ircmd: parseInt( actionCode, 16).toString()})
      this.appSocketService.sendMsg(soundEvent)

    }

  }