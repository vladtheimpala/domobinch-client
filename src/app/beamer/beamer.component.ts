import { Component, Input, OnInit } from '@angular/core';
import { AppSocketService } from '@app/app-socket.service';
import { BeamerActionCode, BeamerEvent } from '../app-models.model';

@Component({
  selector: 'app-beamer',
  templateUrl: './beamer.component.html',
  styleUrls: ['./beamer.component.scss']
})
export class BeamerComponent implements OnInit {
  actions: any[];

  @Input() isReady: boolean;
  loaded: boolean;


  constructor(private appSocketService: AppSocketService) {
    this.actions = [
      { code: BeamerActionCode.TURN_ON, icon: 'power_settings_new'},
      { code: BeamerActionCode.TURN_ON, icon: 'power_settings_new'}


    ]
  }
    ngOnInit() {
      this.loaded = true;
    }

    sendAction(actionCode) {

      console.error('sound action sent:', actionCode)

      const soundEvent = new BeamerEvent({command: actionCode})
      this.appSocketService.sendMsg(soundEvent)

    }

  }