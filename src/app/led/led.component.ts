import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { filter, map, tap } from 'rxjs/operators';
import { BrightnessEvent, Color, ColorEvent, EventName } from '../app-models.model';
import { AppSocketService } from '../app-socket.service';


@Component({
    selector: 'binch-led',
    templateUrl: './led.component.html',
    styleUrls: ['./led.component.scss']
  })
  export class LedComponent implements OnInit {
    colors: Color[];
    selectedColor: any; // or color.selected
    private readonly OFF_COLOR_HEX = '#000000';

    brightnessValues = {
      d1: 0,
      d2: 0
    }
    
    // birghtness
    constructor(private appSocketService: AppSocketService, private renderer: Renderer2, private el: ElementRef) {
      this.colors = [
        {id:-1,hexValue: this.OFF_COLOR_HEX, name:'black'},
        {id:1,hexValue:'#FFFFFF', name: 'red'},
        {id:3,hexValue:'#00FF00', name: 'dsf'},
        {id:4,hexValue:'#0000FF', name: 'fsd'},
        {id:5,hexValue:'#FFFF00', name: 'ss'},
        {id:6,hexValue:'#00FFFF', name: 'aa'},
        {id:7,hexValue:'#FF00FF', name: 'aa'},
        {id:8,hexValue:'#c90000', name: 'fsd'},
        {id:9,hexValue:'#fda600', name: 'ss'},
        {id:10,hexValue:'#32ae00', name: 'aa'},
        {id:11,hexValue:'#0038ff', name: 'aa'},
        {id:12,hexValue:'#FFFFFF', name: 'aa'}
      ];




    const cols = [
"00000F",
"0000FF",
"000F0F",
"000FFF",
"00F00F",
"00F0F0",
"00FF00",
"00FFF0",
"0F0000",
"0F00F0",
"0F0F00",
"0F0FF0",
"0FF000",
"0FF0F0",
"0FFF00",
"0FFFF0",
"F00000",
"F000F0",
"F00F00",
"F00FFF",
"F0F00F",
"F0F0FF",
"F0FF0F",
"F0FFFF",
"FF0000",
"FF00F0",
"FF0F00",
"FF0FF0",
"FFF000",
"FFF0F0",
"FFFF00",
"FFFFF0"]

  // cols.forEach((colHex, index) => {
  //   this.colors.push(
  //     {
  //       id: index+15,
  //       hexValue: `#${colHex}`,
  //       name: "other"
  //     }
  //   );
  // });

}


    ngOnInit() {
      const colorSubscription = this.appSocketService.events$
        .pipe(
          filter((eventObject: ColorEvent) => !!eventObject && eventObject.event === EventName.COLOR),
          filter((eventObject: ColorEvent) => !!eventObject.payload && !!eventObject.payload.id),
          map(eventObject => this.colors.find(color => color.id === eventObject.payload.id)),
          filter(color => !!color),
          tap(color => {
            this.selectedColor = color;
            this.renderer.setStyle(
              document.body,
              'background-color',
              this.selectedColor.hexValue
            );
          }),
          tap(() => console.error('selected:', this.selectedColor))
      )
      .subscribe();

      const brightnessSubscription = this.appSocketService.events$
        .pipe(
          filter((eventObject: BrightnessEvent) => !!eventObject && eventObject.event === EventName.BRIGHTNESS),
          filter((eventObject: BrightnessEvent) => !!eventObject && !!eventObject.payload && !!eventObject.payload.d1 && !!eventObject.payload.d2),
          // map(eventObject => eventObject.payload.value as string),
          tap((eventObject: BrightnessEvent) => {

            this.brightnessValues['d1'] = parseInt(eventObject.payload.d1 as any);
            this.brightnessValues['d2'] = parseInt(eventObject.payload.d2 as any);

              // this.brightnessValues = Object.values(eventObject.payload)
              //   .reduce((device, index) =>{
              //   console.log(device, index)
              //     this.brightnessValues[index] = device, []
              //   });
              //   console.log(this.brightnessValues)
              // this.brightnessValueD = parseInt(eventObject.payload.value as string);
            
          
          })
      )
      .subscribe();
    }

    setBrightness(event, device) {
      const brightnessEvent = new BrightnessEvent(
          {d1: this.brightnessValues["d1"], 
          d2: this.brightnessValues["d2"]}
        );
      this.appSocketService.sendMsg(brightnessEvent);
    }


    chooseColor(color: Color) {
      const payload = {
        id: color.id,
        ...this.computeRgb(color.hexValue),
        
      };
      this.sendMessage(new ColorEvent(payload));
    }

    sendMessage(event: ColorEvent) {
      if (!event && !event.event) {
        return;
      }
      console.error('will be sent', event)
      this.appSocketService.sendMsg(event);
    }

    computeRgb(hex,opacity = 1): any{
      hex = hex.replace('#','');
      let r = parseInt(hex.substring(0,2), 16);
      let g = parseInt(hex.substring(2,4), 16);
      let b = parseInt(hex.substring(4,6), 16);
      return ({r,g,b});
  }

  test(event) {
    console.error('event', event)
  }

  
}
