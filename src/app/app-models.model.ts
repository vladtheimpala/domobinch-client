
// **************************
// *****| MAIN
// **************************
export interface BinchEvent {
    event: EventName,
    payload: any
}
export enum EventName {
    COLOR = 'COL',
    BRIGHTNESS = 'BRI',
    FAN = 'FAN',
    WIFI = 'WIF',
    SOUND = 'SOU',
    BEAMER = 'BEA',
    READY = 'RED'

}

// **************************
// *****| WIFI
// **************************
export class WifiEvent implements BinchEvent {
    event: EventName.WIFI;
    payload: WifiEventPayload;
    constructor(
        payload
    ) {
        this.event = EventName.WIFI;
        this.payload = payload;
    }
}
export interface WifiEventPayload {
    value: boolean; // [0,1]
}
// **************************
// *****| COLOR
// **************************
export class ColorEvent implements BinchEvent {
    event: EventName.COLOR;
    payload: ColorEventPayload;
    constructor(
        payload
    ) {
        this.event = EventName.COLOR;
        this.payload = payload;
    }
}
export interface ColorEventPayload {
    id: number; // 0-255
    r: number; // 0-255
    g: number; // 0-255
    b: number; // 0-255
}
export interface Color {
    id: number;
    name: string;
    hexValue: string;
    rgbValue?: string;
    selected?: boolean;
}

// **************************
// *****| BRIGHTNESS
// **************************
export class BrightnessEvent implements BinchEvent {
    event: EventName.BRIGHTNESS;
    payload: BrightnessEventPayload;
    constructor(
        payload
    ) {
        this.event = EventName.BRIGHTNESS;
        this.payload = payload;
    }
}
export interface BrightnessEventPayload {
    d1: number|string; // 0-100,
    d2: number|string; // 0-100
    
}

// **************************
// *****| BEAMER
// **************************
export interface BeamerEventPayload {
    command: string; // command
}
export enum BeamerActionCode {
   // TEST
   TURN_ON = 'TURN_ON',
   TURN_OFF = 'TURN_OFF'

}
export class BeamerEvent implements BinchEvent {
    event: EventName.BEAMER;
    payload: BeamerEventPayload;
    constructor(
        payload
    ) {
        this.event = EventName.BEAMER;
        this.payload = payload;
    }
}

// **************************
// *****| SOUND
// **************************
export interface SoundEventPayload {
    ircmd: string; // command
}
export enum SoundActionCode {
    // VOLUME
    VOL_UP = '400555AA',
    VOL_DOWN = '400556A9',
    // SOURCE 
    SOURCE_BT = '400542BD',
    SOURCE_HDMI = '400541BE',
    // TURNO ON/OFF
    TOGGLE_TURN_ON_OFF = '400501FE',
    // MUTE
    MUTE = '400557A8'
}
export class SoundEvent implements BinchEvent {
    event: EventName.SOUND;
    payload: SoundEventPayload;
    constructor(
        payload
    ) {
        this.event = EventName.SOUND;
        this.payload = payload;
    }
}

// **************************
// *****| FAN
// **************************
export interface FanEventPayload {
    value: number; // 0-100
}

// 
export class ReadyEvent implements BinchEvent {
    event: EventName.READY;
    payload: { value: boolean };
    constructor(
        payload
    ) {
        this.event = EventName.READY;
        this.payload = payload;
    }
}



export const ENV = 'localhost:1680';
// const ENV = '192.168.1.123:1680';

// const ENV = 'oboro.ch';

export const GET_STATE = `http://${ENV}/binch/public/status`;
export const POST_STATE = `http://${ENV}/binch/public/set/`;

export const POS_LCOAL = { latitude: 46.7101421, longitude: 6.7986414 };


