import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';
import { tap, filter, map, share, shareReplay } from 'rxjs/operators';
import { BinchEvent, EventName } from './app-models.model';

@Injectable()
export class AppSocketService {
  
  private messages: Subject<any>;
  events$: Observable<BinchEvent>;
  
  // Our constructor calls our wsService connect method
  constructor(private wsService: WebsocketService) {
    this.messages = <Subject<any>>wsService
      .connect()
      .map((response: any): any => {
        return response;
      });

    // filtering
    this.events$ = this.messages.asObservable()
      .pipe(
        tap(data => console.error('received:',data)),
        filter((data) => !!event),
        map(data => JSON.parse(data)),
        filter((data: BinchEvent) => !!data && !!data.event && Object.values(EventName).includes(data.event)),
        share()
      );
   }
  
  // Our simplified interface for sending
  // messages back to our socket.io server
  sendMsg(msg) {
    this.messages.next(msg);
  }

}