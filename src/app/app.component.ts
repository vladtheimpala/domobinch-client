import { HttpClient } from '@angular/common/http';
import { Component, HostListener, NgZone, OnInit } from '@angular/core';
import 'hammer-timejs';
import 'hammerjs';
import { Observable, of } from 'rxjs';
import { filter, map, shareReplay, tap } from 'rxjs/operators';
import { EventName, POS_LCOAL, ReadyEvent } from './app-models.model';
import { AppSocketService } from './app-socket.service';
import { PARTICLES_OPTIONS } from './particles';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'Heenok';
  state: boolean;
  loading: boolean;
  hide: boolean;
  audioUrl: string;
  showWifi: boolean;
  showLed: boolean;
  isLoading: boolean;
  error: string;

  isConnected$: Observable<boolean>;
  getPos$: Observable<Position>;

  val: string;

  shiftPressed: boolean;
  showDebug: boolean;

  pass: number;

  myStyle: object = {};
  myParams: object = {};
  myWidth: number = 100;
  myHeight: number = 100;

  isReady$;

  constructor(private http: HttpClient, private _ngZone: NgZone, private appSocketService: AppSocketService) {
    // empty
  }
  ngOnInit() {


    // let p1 = new Promise((res, rej) => {
    //   setTimeout(() => {
    //     console.error('timout')
    //     res(1);
    //   }, 2000)
    // })

    // p1.then(
    //   (val) => {console.error('then',val);return Error("3")}
    // )
    // .then(
    //   val => {
    //     console.error(val)
    //   },
    //   val => {console.error('error', val)}
    // )



    this.myStyle = {
        'position': 'fixed',
        'width': '100%',
        'height': '100%',
        'z-index': -1,
        'top': 0,
        'left': 0,
        'right': 0,
        'bottom': 0,
    };

    this.myParams = PARTICLES_OPTIONS;

    // gps
    this.getPos$ = Observable.create(observer => {
        navigator.geolocation.getCurrentPosition(
            position => {
                observer.next(position);
                observer.complete();
            },
            observer.error.bind(observer)
        );
    });

    console.log('init') // filter or event swtich birghtness vs color



    this.isReady$ = this.appSocketService.events$
      .pipe(
        tap(res => console.error('rr', res)),
        filter((eventObject: ReadyEvent) => !!eventObject && eventObject.event === EventName.READY),
        filter((eventObject: ReadyEvent) => !!eventObject.payload && eventObject.payload.value !== undefined),
        map(eventObject => eventObject.payload.value),
        shareReplay(1)
      )
    

  }



  connect() {
    console.error(this.pass)
    if (this.pass === 9286) {
      this.isConnected$ = of(true);
      this.showError(undefined);
    } else {
      this.isConnected$ = of(false);
      this.showError('wrong');
    }

    // this.isLoading = true;
    // this.isConnected$ = this.getPos$
    //   .pipe(
    //     first(),
    //     map((position) => this.isNear(position)),
    //     tap(authorized => {
    //       if (!authorized) {
    //         this.showError('too far');
    //       }
    //     }),
    //     tap(() => this.isLoading = false),
    //     share()
    //   )
      //.subscribe();
  }

  @HostListener('window:keyup', ['$event'])
  shouldShowDebug(event) {
    if (event.keyCode == 16) {
      this.shiftPressed = false; 
      return;
    }   
    if (event.keyCode == 84) {
      console.log("magic happens");
      this.showDebug = !this.showDebug;
      this.isConnected$ = of(true);
    }
  }

  @HostListener('window:keydown', ['$event']) 
  setShiftPressed() {
    this.shiftPressed = true;
  }



  private showError(name: string) {
    this.error = name;
  }

  sendVal() {
    console.log('will be sent:',this.val)
    //const val = JSON.parse(this.val)
    this.appSocketService.sendMsg(this.val);
  }

  hideAll() {
    this.showWifi = false;
    this.showLed = false;
  }

  private isNear(position: Position) {

    const lat = position.coords.latitude;
    const lng = position.coords.longitude;
    const d = this.getDistanceFromLatLonInKm(lat, lng, POS_LCOAL.latitude, POS_LCOAL.longitude);
    console.error('dist', d)
    return (d < 1);

  }

  private getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }
  
  private deg2rad(deg) {
    return deg * (Math.PI/180)
  }





}
