import { NgModule } from '@angular/core';
// import { MatColorPickerModule } from 'mat-color-picker';
// import {} from '@angular/material/slide-toggle';
import { ColorPickerModule } from 'ngx-color-picker';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatSlideToggleModule,
  MatChipsModule,
  MatGridListModule,
  MatProgressSpinnerModule,
  MatSliderModule,
  MatInputModule
  
  
} from '@angular/material';

@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSlideToggleModule,
    ColorPickerModule,
    MatChipsModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatInputModule
  
    
    // MatColorPickerModule
  ],
  exports: [
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatSlideToggleModule,
    ColorPickerModule,
    MatChipsModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatSliderModule,
    MatInputModule
    // MatColorPickerModule
  ]
})
export class MaterialModule {}