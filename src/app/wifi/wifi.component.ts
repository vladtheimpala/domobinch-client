import { Component, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { filter, map, tap } from 'rxjs/operators';
import { EventName, WifiEvent } from '../app-models.model';
import { AppSocketService } from '../app-socket.service';

@Component({
    selector: 'binch-wifi',
    templateUrl: './wifi.component.html',
    styleUrls: ['./wifi.component.scss']
  })
  export class WifiComponent {

    title = 'Heenok';
    state: boolean = false;
    loading: boolean;
    hide: boolean;
    audioUrl: string;
    showWifi: boolean;
    showLed: boolean;

    constructor(private appSocketService: AppSocketService, private http: HttpClient, private _ngZone: NgZone) {
      // empty
    }
    ngOnInit() {
      const wifiSubscription = this.appSocketService.events$
        .pipe(
            filter((eventObject: WifiEvent) => !!eventObject && eventObject.event === EventName.WIFI),
            filter((eventObject: WifiEvent) => !!eventObject.payload && eventObject.payload.value !== undefined),
            map(eventObject => eventObject.payload.value),
            tap(state => {
              this.state = state;
            })
        )
        .subscribe();
    }

    swipeToggle(event) {
      const wifiEvent = new WifiEvent({value: !this.state });
      this.appSocketService.sendMsg(wifiEvent);
    }
  }

  