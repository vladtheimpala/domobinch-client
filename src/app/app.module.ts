import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ParticlesModule } from 'angular-particle';
import { AppSocketService } from './app-socket.service';
import { AppComponent } from './app.component';
import { BeamerComponent } from './beamer/beamer.component';
import { LedComponent } from './led/led.component';
import { MaterialModule } from './material.module';
import { SoundComponent } from './sound/sound.component';
import { WebsocketService } from './websocket.service';
import { WifiComponent } from './wifi/wifi.component';



@NgModule({
  declarations: [
    AppComponent,
    WifiComponent,
    LedComponent,
    SoundComponent,
    BeamerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    ParticlesModule
    
  ],
  providers: [WebsocketService, AppSocketService],
  bootstrap: [AppComponent]
})
export class AppModule { }

